/*jshint esversion: 6*/
/*jshint -W033*/

// History Toggle
document.querySelectorAll('h3').forEach(function(el) {
  el.onclick = function(e) {
    let selector = e.target.nextElementSibling
    let selector2 = e.target.firstElementChild.firstElementChild
    theToggle(selector)
    theRotate(selector2)
  }
})

document.querySelectorAll('h3>span').forEach(function(el) {
  el.onclick = function(e) {
    let selector = e.target.parentElement.nextElementSibling
    let selector2 = e.target.firstElementChild
    theToggle(selector)
    theRotate(selector2)
  }
})

const theToggle = function(el) {
  el.classList.toggle('open')
}

const theRotate = function(el) {
  el.classList.toggle('spin')
}
