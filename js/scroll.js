/*jshint esversion: 6*/
/*jshint -W033*/

$(document).ready(function() {
  $('.scroll').click(function() {
    $('html, body').animate({scrollTop: $('#introduction').offset().top}, 300)
  })
  $('.1').click(function() {
    $('html, body').animate({scrollTop: $('#introduction').offset().top}, 300)
  })

  $('.2').click(function() {
    $('html, body').animate({scrollTop: $('#chronological-table').offset().top}, 300)
  })

  $('.3').click(function() {
    $('html, body').animate({scrollTop: $('#risk-management').offset().top}, 300)
  })
})
