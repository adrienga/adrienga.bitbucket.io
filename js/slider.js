/*jshint esversion: 6*/
/*jshint -W033*/

fetch('../json/post.json')
.then(res => res.json())
.then(data => {
  data.forEach(post => {
    document.querySelector('.portfolio-display').innerHTML +=  `
    <div class="display-content">
      <img src="${post.image}">
      <div class="display-text-box">
        <h4>${post.title}</h4>
        <p>${post.text}</p>
      </div>
      <div class="portfolio-button">
        ${post.button}
      </div>
    </div>
    `
    document.querySelector('.portfolio-nav').innerHTML += `
    <div class="nav-content">
      <img src="${post.image}">
    </div>
    `
  })
})
.then(() => {
  $('.portfolio-display').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.portfolio-nav',
  })
  $('.portfolio-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.portfolio-display',
    dots: true,
    centerMode: true,
    focusOnSelect: true,
    prevArrow: '<i class="fa fa-chevron-left"></i>',
    nextArrow: '<i class="fa fa-chevron-right"></i>',
  })
})
