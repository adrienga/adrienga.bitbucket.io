/*jshint esversion: 6*/
/*jshint -W033*/

const topBtn = document.querySelector('#top-button')

window.onscroll = function() {scrollFunction()}

function scrollFunction() {
  if(document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    topBtn.style.display = 'block'
  } else {
    topBtn.style.display = 'none'
  }
}

document.querySelector('#top-button').addEventListener('click', function topFunction() {
  $('html, body').animate({scrollTop: $('header').offset().top}, 300)
  // document.body.scrollTop = 0;
  // document.documentElement.scrollTop = 0;
})
