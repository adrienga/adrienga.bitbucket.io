/*jshint esversion: 6*/
/*jshint -W033*/

$(document).ready(function() {
  $('.waypoints').waypoint(function(direction) {
    if(direction === 'down') {
      $('header>nav').css('background-color', 'rgba(255, 255, 255, 1)')
      $('.about').css('background-color', 'rgba(255, 255, 255, 1)')
    } else {
      $('header>nav').css('background-color', 'rgba(255, 255, 255, .7)')
      $('.about').css('background-color', 'rgba(255, 255, 255, .7)')
    }
  }, {
    offset: '60px'
  })
})
